﻿using SignalConvert.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SignalConvert
{
    public partial class Form1 : Form
    {
        #region Private Methods

        private void convertRickToCheck(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(' ').ToList();

                    string data = dtDia.Value.ToString("yyyy-MM-dd");

                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();

                    string action = sinal[2].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Replace("M", string.Empty);

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutputToCheck(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertCatalogados(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(',').ToList();

                    string data = dtDia.Value.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();
                    string action = sinal[2].Trim();
                    string tempo = "M" + sinal[3];

                    sinais.Add(new Sinal
                    {
                        Action = action.ToUpper(),
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).OrderBy(o => o.ParMoeda).ToList();

                makeOutputToCheck(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertCatalogadosRobo(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(' ').ToList();

                    string data = DateTime.Parse(sinal[0]).ToString("yyyy-MM-dd");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[2].Trim();
                    string action = sinal[3].Trim();
                    string tempo = sinal[4].Replace("M", string.Empty);

                    sinais.Add(new Sinal
                    {
                        Action = action.ToUpper(),
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();

                makeOutputToCheck(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertRoboTelegram(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(' ').ToList();


                    string data = DateTime.Now.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[2].Trim();
                    string action = sinal[3].Trim();
                    string tempo = sinal[4].Replace("M", string.Empty);

                    sinais.Add(new Sinal
                    {
                        Action = action.ToUpper(),
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutputRoboTelegram(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertCatalogadosRoboGold(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(';').ToList();

                    string pos1 = sinal[0];
                    string pos2 = sinal[1];

                    var pos1Splited = pos1.Split('-').ToList();
                    var pos2Splited = pos2.Split('-').ToList();

                    string data = DateTime.Now.ToString("yyyy-MM-dd");
                    string parMoedas = pos1Splited[1].Trim();
                    string hora = pos2Splited[0].Trim();
                    string action = pos2Splited[1].Trim();
                    string tempo = pos1Splited[0].Replace("M", string.Empty);

                    sinais.Add(new Sinal
                    {
                        Action = action.ToUpper(),
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutputToCheck(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertSimples(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(';').ToList();
                    string data = dtDia.Value.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();
                    string action = sinal[2].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();

                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertV1(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split('-').ToList();
                    string data = dtDia.Value.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();
                    string action = sinal[2].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();

                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertVipHtml(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(',').ToList();
                    string data = dtDia.Value.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();
                    string action = sinal[2].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();

                sinais = sinais.GroupBy(x => x.Hora).Select(y => y.First()).ToList();

                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertVipHtmlRoboGold(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(',').ToList();
                    string data = dtDia.Value.ToString("dd/MM/yyyy");
                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[0].Trim();
                    string action = sinal[2].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                sinais = sinais.GroupBy(x => x.Hora).Select(y => y.First()).ToList();

                makeOutputGold(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertV2(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(',').ToList();

                    string data = dtDia.Value.ToString("dd/MM/yyyy");

                    string parMoedas = sinal[4].Trim();
                    string hora = sinal[3].Trim();

                    string action = sinal[5].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertPremiun(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(';').ToList();

                    string data = dtDia.Value.ToString("dd/MM/yyyy");

                    string parMoedas = sinal[0].Trim();
                    string hora = sinal[2].Trim();

                    string action = sinal[3].Trim();
                    string tempo = "M" + sinal[4].Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertPremiun2(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(',').ToList();

                    string data = dtDia.Value.ToString("dd/MM/yyyy");

                    string parMoedas = sinal[4].Trim();
                    string hora = sinal[3].Trim();

                    string action = sinal[5].Trim();
                    string tempo = cbTempoVela.SelectedItem.ToString().Trim();

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertPremiunToCheck(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    var sinal = item.Split(';').ToList();

                    string data = dtDia.Value.ToString("yyyy-MM-dd");

                    string parMoedas = sinal[0].Trim();
                    string hora = sinal[2].Trim();

                    string action = sinal[3].Trim();
                    string tempo = sinal[4];

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutputToCheck(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void convertPremiunVIP(List<String> lines)
        {
            try
            {
                List<Sinal> sinais = new List<Sinal>();

                foreach (var item in lines)
                {
                    string linha = item.Replace("⏳", "").Replace("🟩", "").Replace("✅", "");

                    var sinal = linha.Split(' ').ToList();

                    string data = dtDia.Value.ToString("dd/MM/yyyy");

                    string parMoedas = sinal[1].Trim();
                    string hora = sinal[2].Trim();

                    string action = sinal[3].Trim();
                    string tempo = sinal[0];

                    sinais.Add(new Sinal
                    {
                        Action = action,
                        Data = data,
                        Hora = TimeSpan.Parse(hora),
                        ParMoeda = parMoedas,
                        TempoVela = tempo
                    });
                }

                sinais = sinais.OrderBy(o => o.Hora).ToList();
                makeOutput(sinais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void makeOutputGold(List<Sinal> sinais)
        {
            foreach (var sinal in sinais)
            {
                string hora = sinal.Hora.ToString().Substring(0, sinal.Hora.ToString().Length - 3);
                this.Output += $"{sinal.ParMoeda};{sinal.DataTime.Day};{hora};{sinal.Action};{sinal.TempoVela.Replace("M", "")}\n";
            }
        }

        private void makeOutput(List<Sinal> sinais)
        {
            foreach (var sinal in sinais)
            {
                this.Output += $"{sinal.Data} {sinal.ParMoeda} {sinal.Hora} {sinal.Action} {sinal.TempoVela} \n";
            }
        }

        private void makeOutputToCheck(List<Sinal> sinais)
        {
            foreach (var sinal in sinais)
            {
                this.Output += $"{sinal.Data} {sinal.Hora},{sinal.ParMoeda},{sinal.Action},{sinal.TempoVela} \n";
            }
        }

        private void makeOutputRoboTelegram(List<Sinal> sinais)
        {
            foreach (var sinal in sinais)
            {
                this.Output += $"/add {sinal.Hora} {sinal.ParMoeda} 5 {sinal.Action} {sinal.TempoVela} \n";
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertV1(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.Output);
            lblError.Text = "Copiado!";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://omegainc.com.br");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertV2(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertPremiun(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertPremiun2(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertPremiunVIP(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void btnConvert_Click_1(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertV1(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertPremiunToCheck(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertRickToCheck(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertSimples(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertCatalogados(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertCatalogadosRobo(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertVipHtml(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                if (String.IsNullOrEmpty(cbTempoVela.SelectedItem.ToString()))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe o tempo de vela";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertVipHtmlRoboGold(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "txt files (*.txt)|*.txt";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, rchOutput.Text);
                }
            }
        }

        #endregion Private Methods

        #region Public Properties

        public String Output { get; set; }

        #endregion Public Properties

        #region Public Constructors

        public Form1()
        {
            InitializeComponent();
            cbTempoVela.DropDownStyle = ComboBoxStyle.DropDownList;            
        }

        #endregion Public Constructors

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertCatalogadosRoboGold(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                var values = rchInput.Text;

                if (String.IsNullOrEmpty(values))
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Informe a lista de sinais";
                    return;
                }

                List<string> lines = values.Split('\n').ToList();

                if (lines.Count == 0)
                {
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Text = "Nenhum sinal encontrado nos dados fornecidos";
                    return;
                }

                this.Output = "";

                this.convertRoboTelegram(lines);

                rchOutput.Text = "";
                rchOutput.Text = this.Output;

                lblError.ForeColor = System.Drawing.Color.Green;
                lblError.Text = "OK!";
            }
            catch (Exception ex)
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = ex.Message;
            }
        }
    }
}