﻿using System;

namespace SignalConvert.Model
{
    public class Sinal
    {
        #region Public Properties

        public string ParMoeda { get; set; }

        public TimeSpan Hora { get; set; }

        public string Action { get; set; }

        public string TempoVela { get; set; }

        public string Data { get; set; }

        public DateTime DataTime
        {
            get => DateTime.Parse(Data);
        }

        #endregion Public Properties
    }
}